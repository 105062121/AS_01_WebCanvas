var minrad = 12;
var maxrad = 96;
var currrad = document.getElementById('radval');
var raddec = document.getElementById('raddec');
var radinc = document.getElementById('radinc');

var setrad = function(newrad){
  // update size
  if(newrad<minrad) newrad=minrad;
  else if(newrad>maxrad) newrad=maxrad;
  else{
    radius = newrad;
    context.lineWidth = radius*2;

    // update label
    currrad.textContent = radius;
  }
}

raddec.addEventListener('click', function(){
  setrad(radius-4);
});
radinc.addEventListener('click', function(){
  setrad(radius+4);
});