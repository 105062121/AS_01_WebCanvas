# Software Studio 2018 Spring Assignment 01 Web Canvas

## Basic Components
* Brush and eraser (White colour as eraser)
* 8 preset colours and custom colours
* Brush size can be configured (Range from 12 to 96)
* Able to input text
* The cursor would change according to the currently used tool
* Able to reset canvas

## Advance Tools
* 2 shapes available (Circle and square)
* Able to download current canvas

## Commands List
1. **Reset Canvas:** /reset
    - Clear the canvas
2. **Download Canvas:** /save
    - Transparent .png file
3. **Set Custom Colours with RGB Value:** /rgb [r] [g] [b]
    - Values range from 0 to 255, integer
4. **Set Custom Colours with Colour Name:** /colour [colour_name]
    - For valid colour names, visit: https://www.w3schools.com/tags/ref_colornames.asp
5. **Input text:** /text [pos_x] [pos_y] [size] [typeface] [contents]
    - The canvas is 900 x 675
    - The [pos_x] [pos_y] would be the lower left point of the input contents
    - Size ranges from 12 to 420, integer
    - If the typeface has space in it, replace them with "_" (ex. Comic_Sans_MS)
    - If the typeface does not exist, it would be given "Times New Romans"

* **Illegal commands would be ignored or be given default value**