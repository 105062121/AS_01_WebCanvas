var textbox = document.getElementById('cmd');
textbox.addEventListener('keyup', function(event){
  event.preventDefault();
  if(event.keyCode == 13){ // Enter
    var command = textbox.value;
    execute(command);
  }
});

function execute(cmd){
  if(cmd=='/reset'){ // command: reset
    context.clearRect(0, 0, canvas.width, canvas.height);
    textbox.value = "";
    return;
  }
  else if(cmd=='/save'){ // command: save
    window.location = canvas.toDataURL('image/png');
    textbox.value = "";
    return;
  }
  else if(cmd[0]=='/'&&cmd[1]=='t'&&cmd[2]=='e'&&cmd[3]=='x'&&cmd[4]=='t'){ // command: text
    textcmd(cmd);
    textbox.value = "";
    return;
  }
  else if(cmd[0]=='/'&&cmd[1]=='r'&&cmd[2]=='g'&&cmd[3]=='b'){ // command: rgb
    rgbcmd(cmd);
    textbox.value = "";
    return;
  }
  else if(cmd[0]=='/'&&cmd[1]=='c'&&cmd[2]=='o'&&cmd[3]=='l'&&cmd[4]=='o'&&cmd[5]=='u'&&cmd[6]=='r'){ // command: colour
    colourcmd(cmd);
    textbox.value = "";
    return;
  }
  else{
    textbox.value = "";
    return;
  }
}

function textcmd(cmd){ // command: text
  var pos_x = "",
      pos_y = "",
      size = "",
      typeface = "",
      contents = "";

  var i = 6;
  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) pos_x += cmd[i++];
  if(parseInt(pos_x)=="NaN") return;

  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) pos_y += cmd[i++];
  if(parseInt(pos_y)=="NaN") return;
  
  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) size += cmd[i++];
  if(parseInt(size)=="NaN") size = 20;
  else if(parseInt(size)>420) size = 420;
  else if(parseInt(size)<12) size = 12;

  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) typeface += cmd[i++];
  // if typeface does not exist, it would be automatically ignored
  
  while(cmd[i]==' ') i++; // ignore space
  while(i<cmd.length) contents += cmd[i++];

  printtext(pos_x, pos_y, size, typeface, contents);
}

function printtext(pos_x, pos_y, size, typeface, contents){
  var canvas = document.getElementById('canvas');
  var context = canvas.getContext('2d');

  var font = "";
  for(var i=0; i<typeface.length; i++){
    if(typeface[i]=='_') font += " ";
    else font += typeface[i];
    continue;
  }
  try{
    context.font = size + "px " + font;
  }
  catch(err){
    context.font = size + "px Times New Romans";
  }
  context.fillText(contents, parseInt(pos_x), parseInt(pos_y)+parseInt(size));
}

function rgbcmd(cmd){ // command: rgb
  var r = "",
      g = "",
      b = "";

  var i = 5;
  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) r += cmd[i++];
  if(parseInt(r)=="NaN") return;

  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) g += cmd[i++];
  if(parseInt(g)=="NaN") return;

  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) b += cmd[i++];
  if(parseInt(b)=="NaN") return;

  var red = parseFloat(r);
      green = parseFloat(g);
      blue = parseFloat(b);
  if(Number.isInteger(red) && Number.isInteger(green) && Number.isInteger(blue)){
    if(red<0 || green<0 || blue<0 || red>255 || green>255 || blue>255) return; // check if 0~255
    else setrgb(r, g, b); // pass string instead of integer
  }
  else return;
}

function setrgb(r, g, b){
  var rgbset = "rgb(" + r + "," + g + "," + b + ")";
  context.fillStyle = rgbset;
  context.strokeStyle = rgbset;
  document.getElementById('rgb').style.backgroundColor = rgbset;
}

function colourcmd(cmd){ // command: colour
  var colour = "";

  var i = 8;
  while(cmd[i]==' ') i++; // ignore space
  while(cmd[i]!=' ' && i<cmd.length) colour += cmd[i++];
  setcolour(colour);
}

function setcolour(colour){
  try{
    context.fillStyle = colour;
    context.strokeStyle = colour;
    document.getElementById('rgb').style.backgroundColor = colour;
  }
  catch(err){
    return;
  }
}