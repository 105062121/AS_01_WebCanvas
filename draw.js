var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var radius = 12; // default
context.lineWidth = radius*2; // make the line smooth

var shape = "circle";
function swap(){
  if(shape == "circle"){
    shape = "square";
    document.getElementById('shape').innerHTML = "Square";
  }
  else{
    shape = "circle";
    document.getElementById('shape').innerHTML = "Circle";
  }
}

var dragging = false; // whether the mouse button is held down, assumed not when loading page
var begin = function(event){
  dragging = true;
  putdot(event);
}
var putdot = function(event){
  if(dragging){
    context.lineTo(event.offsetX, event.offsetY);
    context.stroke(); // connecting the dots
    //
    context.beginPath();
    //
    if(shape == "circle"){
      context.arc(event.offsetX, event.offsetY, radius, 0, Math.PI*2);
      context.fill();
    }
    else if(shape == "square"){
      context.fillRect(event.offsetX-radius, event.offsetY-radius, radius*2, radius*2);
    }
    //
    context.beginPath();
    context.moveTo(event.offsetX, event.offsetY);
  }
  else return;
}
var end = function(){
  dragging = false;
  context.beginPath(); // starting a new path so that all draws are individual
}

canvas.addEventListener('mousedown', begin);
canvas.addEventListener('mousemove', putdot);
canvas.addEventListener('mouseup', end);
canvas.addEventListener('mouseleave', end);